#! /usr/bin/python


'''
    Simple socket server using threads
'''
 
import socket
import sys
import getopt
import select
import signal

 
HOST = ''		# Symbolic name, meaning all available interfaces
PORT = 8888		# Arbitrary non-privileged port
 
socks = []		# Array of sockets we've created...
ports = [ 8888 ]	# The ports we're connecting to...


# Close all open ports before we exit...

def closeAll(signum, frame):
	print('>>> Closing open sockets')

	for p in socks:
		p.close()

	exit (0)



# Process either a port or a port range..

def parseP(p):
	if p.find("-") != -1:
		result = []
		r = p.split("-")
		s = int(r[0])
		l = int(r[1])

		while s <= l:
			result.append(s)		# Add each port in the range...
			s = s + 1

		return(result)
	else:
		return([ int(p) ])			# Single element list...



# --------------------------------------------------------------------------------------------------------
# M A I N
# --------------------------------------------------------------------------------------------------------

try:
	opts, args = getopt.getopt(sys.argv[1:], "h:p:", ["host=", "port="])
except getopt.GetoptError as err:
	usage(err)
	sys.exit(2)

for opt, arg in opts:
	if opt in ("-p", "--port"):
		ports = []
		[ ports.extend(parseP(p))  for p in arg.split(",") ]
	elif opt in ("-h", "--host"):
		HOST = arg


# Create a socket for each of the ports...

for p in ports:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	# Try to bind...

	try:
		s.bind((HOST, p))
		s.listen(1)
		socks.append(s)

		print('Socket created and bound to ', end='')

		if HOST == '':
			print('port', str(p))
		else:
			print(HOST + ':' + str(p))
	except socket.error as msg:
		print('Bind to', end='')

		if HOST != '':
			print(HOST + ':', end='')
		else:
			print('port ', end='')

		print(str(p) + ' failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
	
     
print('Socket binding complete. Listening...')
signal.signal(signal.SIGINT, closeAll)
 

while socks:
	r, w, e = select.select(socks, [], [])

	for r1 in r:
		for i in socks:
			if r1 == i:
				conn, client = i.accept();
				mesg = 'Thank you and goodnight from ';

				print('Saw connection from ' + client[0] + ':' + str(client[1]) + ' to ', end='')

				if HOST == '':
					print('port ' + str(i.getsockname()[1]))
				else:
					print(HOST + ':' + str(i.getsockname()[1]))

				if HOST == '':
					mesg += 'port ' + str(i.getsockname()[1]) + '\n'
#					conn.send('Thank you and goodnight from port ' + str(i.getsockname()[1]) + '\n', 0);
				else:
					mesg += HOST + ':' + str(i.getsockname()[1]) + '\n'
#					conn.send(b'Thank you and goodnight from ' + HOST + ':' + str(i.getsockname()[1]) + b'\n', 0);

				conn.send(mesg.encode(), 0)

				conn.close()
