Purpose
-------

This is a server which sits on a number of nominated ports waiting for in-coming connections. Upon receipt of a connection
the server will display a message indicating that the message has been received and then will immediately close the connection.
This is designed to test the link between 2 machines. This came out of the need to open a network path which you may not
be able to immediately test with a full-blown application. In this instance the script will be able to confirm that the path
has been correctly set up.

Usage
-----

	server [ -h ip-address ][ -p port-range ]

		-h ip-address ................. Bind to ip-address rather than listening on all addresses
		-p port-range ................. Listen port-range rather than port 8888

		port-range is a comma-separated list of single ports and ranges, start to end separated by dashes

Purpose
-------

The script defaults to listening across all IPs on port 8888, but of course this can be altered via command line arguments.

	$ python ./server.py
	Socket created and bound to port 8888
	Socket binding complete. Listening...

From another terminal (this example is from the same server):

	$ telnet 0 8888
	Trying 0.0.0.0...
	Connected to 0.
	Escape character is '^]'.
	Thank you and goodnight from port 8888
	Connection closed by foreign host.

On the terminal from which the command is started you should see the following:

	Saw connection from 127.0.0.1:37599 to port 8888

If the connection's from a remote host the IP will reflect this. If you don't have telnet installed `nc`, netcat, can be used
instead.

To make life a touch easier you can specify more than one port or group of ports by separating ports and port ranges with commas. A port range
is given as 2 numbers separated using a dash, so the following can be used to indicate 3 sets of values, ports
8888 to 8898, port 30000 and ports 1050 to 1070:

	$ python ./server.py -p 8888-8898,30000,1050-1070
	Socket created and bound to port 8888
	Socket created and bound to port 8889
	Socket created and bound to port 8890
	Socket created and bound to port 8891
	Socket created and bound to port 8892
	Socket created and bound to port 8893
	Socket created and bound to port 8894
	Socket created and bound to port 8895
	Socket created and bound to port 8896
	Socket created and bound to port 8897
	Socket created and bound to port 8898
	Socket created and bound to port 30000
	Socket created and bound to port 1050
	Socket created and bound to port 1051
	Socket created and bound to port 1052
	Socket created and bound to port 1053
	Socket created and bound to port 1054
	Socket created and bound to port 1055
	Socket created and bound to port 1056
	Socket created and bound to port 1057
	Socket created and bound to port 1058
	Socket created and bound to port 1059
	Socket created and bound to port 1060
	Socket created and bound to port 1061
	Socket created and bound to port 1062
	Socket created and bound to port 1063
	Socket created and bound to port 1064
	Socket created and bound to port 1065
	Socket created and bound to port 1066
	Socket created and bound to port 1067
	Socket created and bound to port 1068
	Socket created and bound to port 1069
	Socket created and bound to port 1070
	Socket binding complete. Listening...

If you want to limit the address that the script listens on you can use the `-h` option:

	$ python ./server -h 10.0.1.20
	Socket created and bound to 10.0.1.20:8888
	Socket binding complete. Listening...

Requirements
------------

The script is written using Python 3 and will need superuser privileges if you need to monitor a port below 1024.
